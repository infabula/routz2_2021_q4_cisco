import json
import requests


class RESTfullException(Exception):
    pass


def ask_user_for_numberplate():
    while True:
        numberplate = input("Nummerplaat:\n")
        if not len(numberplate) == 6:
            print("Lengte 6 graag.")
        else:
            return numberplate.upper()


def print_interesting_items(data):
    # data is list
    if data:  # data not empty
        car_props = data[0]  # dictioary
        efficiency = car_props.get("zuinigheidsclassificatie", 'onbekend')

        if efficiency == 'A':
            print("Dat is een hele zuinige bak!")
        elif efficiency == 'B':
            print("Dat is een standaard effeciente bolide.")
        elif efficiency == 'C':
            print("Dat is een slurper.")
        else:
            print("Onbekend efficient.")


def get_rest_data(numberplate):
    base_url = "https://opendata.rdw.nl/resource/m9d7-ebf2.json"
    url = f"{base_url}?kenteken={numberplate}"

    response = requests.get(url)

    if response.status_code == 200:  # success
        data = json.loads(response.text)  # convert to python data type
        return data
    else:
        raise RESTfullException("Could not fetch data from", url)


def main():
    try:
        numberplate = ask_user_for_numberplate()
        data = get_rest_data(numberplate)
        print_interesting_items(data)
    except RESTfullException as e:
        print(e)
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()