import os
import json
from jinja2 import Template
from jinja2 import FileSystemLoader, Environment


templates = Environment(loader=FileSystemLoader('./templates'))

with open('data/party_data.json', 'r') as infile:
    json_content = infile.read()
    data = json.loads(json_content)

'''
with open('data/party_data.json', 'w') as outfile:
    data_as_json = json.dumps(data, indent=2)
    outfile.write(data_as_json)
'''

invite = templates.get_template('invites.j2')
content = invite.render(data)

file_path = os.path.join('output', "invite.txt")

with open(file_path, 'w') as outfile:
    outfile.write(content)
