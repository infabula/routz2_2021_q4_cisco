import paramiko

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname='sandbox-iosxe-latest-1.cisco.com',
            username='developer',
            password='C1sco12345',
            port=22)

command = 'show ip interface brief \n'

shell = ssh.invoke_shell()

(stdin, stdout, stderr) = shell(command)
response = stdout.readlines()

header = "Interface              IP-Address      OK? Method Status"

for line_number, line in enumerate(response, 1):
    print(line.rstrip())
    #if "Interface" in line:
    #    print(line_number, line.rstrip())

#print(' '.join(map(str, response)))

ssh.close()